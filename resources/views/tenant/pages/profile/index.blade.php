@extends('layouts.main')

@section('content')
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12 order-2 order-md-1">
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <div class="info-box bg-light">
                                    <div class="info-box-content">
                                        <span class="info-box-text text-center text-muted">Products Count</span>
                                        <span class="info-box-number text-center text-muted mb-0">{{number_format($user->products_count)}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 ">
                                <h3 class="text-primary"><i class="fas fa-building"></i> {{$user->name}}</h3>
                                <p class="text-muted">{{$user->address}}</p>
                                <br>
                                <div class="text-muted">
                                    <p class="text-sm">Subdomain
                                        <b class="d-block">{{$user->subdomain}}</b>
                                    </p>
                                    <p class="text-sm">Phone
                                        <b class="d-block">{{$user->tel}}</b>
                                    </p>
                                    <p class="text-sm">Email
                                        <b class="d-block">{{$user->email}}</b>
                                    </p>
                                    <p class="text-sm">Status
                                        <b class="d-block"><span class="badge {{$user->status_badge}}">{{$user->formatted_status}}</span></b>
                                    </p>
                                    <p class="text-sm">Creation Date
                                        <b class="d-block">{{$user->created_at->toDateTimeString()}}</b>
                                    </p>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
@stop