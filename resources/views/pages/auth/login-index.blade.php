@extends('layouts.main',['layout'=>'auth'])

@section('content')
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <div class="h1">
                {{--                displaying the current route prefix whether its admin or company--}}
                <b>
                    {{App\Services\MultiTenant::isAdminRoute() ? 'Admin' : 'Company'}}
                </b>
                Dashboard
            </div>
        </div>
        <div class="card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <form action="{{$login_url}}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input type="email" class="form-control" value="{{old('email')}}" required placeholder="Email"
                           name="email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" placeholder="Password" required name="password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember" name="remember_me" value="1">
                            <label for="remember">
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

        </div>
        <!-- /.card-body -->
    </div>
@stop