@extends('layouts.main')
@section('content')
    <section class="content mb-1">
        <form action="{{route('admin.product.store')}}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputName">Product Name</label>
                                <input type="text" id="inputName" value="{{old('name')}}" name="name" required
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="inputPrice">Price</label>
                                <input type="number" id="inputPrice" value="{{old('price')}}" name="price" required
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="inputQuantity">Quantity</label>
                                <input type="number" id="inputQuantity" value="{{old('quantity')}}" name="quantity" required
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="inputCompany">Company</label>
                                <select id="inputCompany" class="form-control custom-select" name="company_id" required>
                                    @foreach($companies as $company)
                                        <option
                                                value="{{$company->id}}"
                                                {{old('company_id') == $company->id ? 'selected' : '' }}>
                                            {{$company->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{route('admin.product.index')}}" class="btn btn-secondary">Cancel</a>
                    <button type="submit" class="btn btn-success float-right"> Create new </button>
                </div>
            </div>
        </form>
    </section>
@stop