@extends('layouts.main')

@section('content')
    <section class="content">

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12 order-2 order-md-1">
                        <div class="row">
                            <div class="col-12 ">
                                <h3 class="text-primary"><i class="fas fa-box-open"></i> {{$product->name}}</h3>
                                <p class="text-muted"><a href="{{route('admin.company.details',$product->company->name)}}">{{$product->company->name}}</a></p>
                                <br>
                                <div class="text-muted">
                                    <p class="text-sm">Price
                                        <b class="d-block">{{number_format($product->price)}}</b>
                                    </p>
                                    <p class="text-sm">Quantity
                                        <b class="d-block">{{number_format($product->quantity)}}</b>
                                    </p>
                                    <p class="text-sm">Creation Date
                                        <b class="d-block">{{$product->created_at->toDateTimeString()}}</b>
                                    </p>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-12">
                        <a href="{{route('admin.product.index')}}" class="btn btn-secondary float-right">Back</a>
                    </div>
                </div>
            </div>
        </div>

    </section>
@stop