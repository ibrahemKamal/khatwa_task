@extends('layouts.main')
@section('content')

    <section class="content">
        <div class="row mb-2 justify-content-between">
            <div class="col-md-4 ">
                <form>
                    <div class="input-group">
                        <input type="search" name="q" value="{{$search_value}}" class="form-control form-control-lg"
                               placeholder="Type your keywords here">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-lg btn-default">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-2">
                <a href="{{route('admin.product.create')}}" type="button" class="btn btn-block btn-primary">Create</a>
            </div>
        </div>

        <div class="card">
            <div class="card-body p-0">
                <table class="table table-striped projects">
                    <thead>
                    <tr>
                        <th style="width: 1%">
                            #
                        </th>
                        <th style="width: 20%">
                            Product Name
                        </th>
                        <th style="width: 30%">
                            Price
                        </th>
                        <th style="width: 8%" class="text-center">
                            Company
                        </th>
                        <th style="width: 20%">
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>
                                {{$product->id}}
                            </td>
                            <td>
                                <a>
                                    {{$product->name}}
                                </a>
                            </td>
                            <td>
                                {{number_format($product->price)}}
                            </td>
                            <td class="project-state">
                                <a href="{{route('admin.company.details',$product->company->name)}}">{{$product->company->name}}</a>
                            </td>
                            <td class="project-actions text-right">
                                <a class="btn btn-primary btn-sm" href="{{route('admin.product.details',$product)}}">
                                    <i class="fas fa-folder">
                                    </i>
                                    View
                                </a>
                                <a class="btn btn-info btn-sm" href="{{route('admin.product.edit',$product)}}">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Edit
                                </a>
                                <button class="btn btn-danger btn-sm delete-row" data-name="{{$product->name}}"
                                        data-url="{{route('admin.product.delete',$product)}}">
                                    <i class="fas fa-trash">
                                    </i>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div class="float-right m-2">
                    {{$products->links()}}
                </div>
            </div>

        </div>

    </section>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-form">
                    @method('delete')
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Confirmation</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="text-bold">
                            Are you sure you want to delete <span id="modal-text" class="text-danger"></span> ?

                            this action cant be reverted !
                        </p>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@stop

@push('scripts')
    <script>
        $('.delete-row').click(function (e) {
            e.preventDefault();
            let url = $(this).data('url');
            let name = $(this).data('name');
            $('#modal-text').html(name)
            $('#delete-form').attr('action', url)
            $('#modal-default').modal('show')
        })
    </script>
@endpush