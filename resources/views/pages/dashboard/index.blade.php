@extends("layouts.main")

@section("content")
    <section class="content">
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-lg-12 col-12">
                    <div class="form-group">
                        <label>Date range:</label>

                        <div class="input-group">
                            <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                            </div>
                            <input type="text" class="form-control float-right" name="date_range" id="reservation">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <section class="col-lg-12 connectedSortable">
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title">Companies Logins / Day</h3>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="barChart"
                                        style=" height: 250px;  max-width: 100%;"></canvas>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </section>
            </div>
        </div>
    </section>
@stop
@push('scripts')
    <script>
        $(function () {
            //Date range picker
            $('#reservation').daterangepicker({
                "startDate": "{{$date_from}}",
                "endDate": "{{$date_to}}",
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, function (start, end, label) {
                let date_from = start.format('YYYY-MM-DD');
                let date_to = end.format('YYYY-MM-DD');
                window.location.search += `&date_from=${date_from}&date_to=${date_to}`
            })
        });
    </script>
    <script>
        const helpers = {
            poolColors: function (a) {
                var pool = [];
                for (let i = 0; i < a; i++) {
                    pool.push('#' + (Math.random() * 0xFFFFFF << 0).toString(16));
                }
                return pool;
            }
        }

        var ctx = document.getElementById("barChart");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: @json($chart_data->get('labels')),
                datasets: [{
                    data:  @json($chart_data->get('data')),
                    backgroundColor: helpers.poolColors({{count($chart_data->get('data'))}}),
                    borderColor:  helpers.poolColors({{count($chart_data->get('data'))}}),
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                        }
                    }]
                },
                legend: {
                    display: false
                },
            }
        });
    </script>
@endpush