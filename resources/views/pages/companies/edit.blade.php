@extends('layouts.main')
@section('content')
    <section class="content mb-1">
        <form action="{{route('admin.company.update',$company)}}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputName">Company Name</label>
                                <input type="text" id="inputName" value="{{old('name') ?? $company->name}}" name="name"
                                       required
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="inputPhone">Phone</label>
                                <input type="number" id="inputPhone" value="{{old('tel') ?? $company->tel}}" name="tel"
                                       required
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="inputDescription">Address</label>
                                <textarea id="inputDescription" class="form-control" rows="4" name="address"
                                          required>{{old('address') ?? $company->address}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="inputStatus">Status</label>
                                <select id="inputStatus" class="form-control custom-select" name="status" required>
                                    @foreach($statuses as $value=>$label)
                                        <option
                                                value="{{$value}}"
                                                {{(old('status') || $company->status == $value) ? 'selected' : ''}}>
                                            {{ucwords($label)}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputSubdomain">Subdomain</label>
                                <input type="text" id="inputSubdomain" name="subdomain" required class="form-control"
                                       value="{{old('subdomain') ?? $company->subdomain}}">
                                <small>must be one word without any spaces or special characters</small>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail">Email</label>
                                <input type="email" id="inputEmail" name="email" required class="form-control"
                                       value="{{old('email') ?? $company->email}}">
                            </div>
                            <div class="form-group">
                                <label for="inputPassword">Password</label>
                                <input type="password" id="inputPassword" name="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="inputPasswordConfirmation">Password Confirmation</label>
                                <input type="password" id="inputPasswordConfirmation" name="password_confirmation"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{route('admin.company.index')}}" class="btn btn-secondary">Cancel</a>
                    <button type="submit" class="btn btn-success float-right" dusk="submit-button"> Update</button>
                </div>
            </div>
        </form>
    </section>
@stop