@extends('layouts.main')
@section('content')

    <section class="content">
        <div class="row mb-2 justify-content-between">
            <div class="col-md-4 ">
                <form>
                    <div class="input-group">
                        <input type="search" name="q" value="{{$search_value}}"  class="form-control form-control-lg"
                               placeholder="Type your keywords here">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-lg btn-default" dusk="submit-search">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-2">
                <a href="{{route('admin.company.create')}}" type="button" dusk="create" class="btn btn-block btn-primary">Create</a>
            </div>
        </div>

        <div class="card">
            <div class="card-body p-0">
                <table class="table table-striped projects">
                    <thead>
                    <tr>
                        <th style="width: 1%">
                            #
                        </th>
                        <th style="width: 20%">
                            Company Name
                        </th>
                        <th style="width: 30%">
                            Subdomain
                        </th>
                        <th style="width: 8%" class="text-center">
                            Status
                        </th>
                        <th style="width: 20%">
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                        <tr>
                            <td>
                                {{$company->id}}
                            </td>
                            <td>
                                <a>
                                    {{$company->name}}
                                </a>
                            </td>
                            <td>
                                {{$company->subdomain}}
                            </td>
                            <td class="project-state">
                                <span class="badge {{$company->status_badge}}">{{$company->formatted_status}}</span>
                            </td>
                            <td class="project-actions text-right">
                                <a class="btn btn-primary btn-sm" href="{{route('admin.company.details',$company)}}" dusk="view">
                                    <i class="fas fa-folder">
                                    </i>
                                    View
                                </a>
                                <a class="btn btn-info btn-sm" href="{{route('admin.company.edit',$company)}}" dusk="edit">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Edit
                                </a>
                                <button class="btn btn-danger btn-sm delete-row" data-name="{{$company->name}}"
                                        dusk="delete"
                                        data-url="{{route('admin.company.delete',$company)}}">
                                    <i class="fas fa-trash">
                                    </i>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div class="float-right m-2">
                    {{$companies->links()}}
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="delete-form">
                    @method('delete')
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Confirmation</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="text-bold">
                            Are you sure you want to delete <span id="modal-text" class="text-danger"></span> ?

                            this action cant be reverted !
                        </p>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger" dusk="delete-button">Delete</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@stop

@push('scripts')
    <script>
        $('.delete-row').click(function (e) {
            e.preventDefault();
            let url = $(this).data('url');
            let name = $(this).data('name');
            $('#modal-text').html(name)
            $('#delete-form').attr('action', url)
            $('#modal-default').modal('show')
        })
    </script>
@endpush