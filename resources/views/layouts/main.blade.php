<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$title ?? 'Khatwa'}}</title>

    <link href="{{asset('css/app.css')}}" rel="stylesheet">

</head>
@if (@$layout ==='auth')

    @include('layouts.partials.auth-layout')

@else

    @include('layouts.partials.default-layout')

@endif

<script src="{{asset('js/app.js')}}"></script>
<script src="{{ mix('js/popper.js') }}"></script>
@stack('scripts')
@include('errors.messages')
</html>
