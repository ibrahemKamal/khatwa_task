<footer class="main-footer">
    <strong>Copyright &copy; 2014-{{now()->year}} <a href="https://khtwah.com/">khtwah.com</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0
    </div>
</footer>
