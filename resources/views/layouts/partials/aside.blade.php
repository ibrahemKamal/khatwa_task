<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block">{{auth()->user()->name}}</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @inject('multiTenant', 'App\Services\MultiTenant')

                @if ($multiTenant::isAdminRoute())
                    <li class="nav-item">
                        <a href="{{route('admin.dashboard.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.company.index')}}" class="nav-link">
                            <i class="nav-icon far fa-building"></i>
                            <p>
                                Companies
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.product.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-box-open"></i>
                            <p>
                                Products
                            </p>
                        </a>
                    </li>
                @else
                    <li class="nav-item">
                        <a href="{{route('companies.homepage.index')}}" class="nav-link">
                            <i class="nav-icon far fa-user-circle"></i>
                            <p>
                                Profile
                            </p>
                        </a>
                    </li>
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
