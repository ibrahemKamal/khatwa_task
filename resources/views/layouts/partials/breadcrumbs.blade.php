<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item {{!count(Request::segments()) ? 'active' : ''}}">
                        @if (count(Request::segments()))

                            <a href="{{route('admin.dashboard.index')}}">Home</a>

                        @else
                            Home
                        @endif
                    </li>
                    <?php $link = "" ?>
                    @for($i = 1; $i <= count(Request::segments()); $i++)
                        @if($i < count(Request::segments()) & $i > 0 & $i < 2)
                            <?php $link .= "/" . Request::segment($i); ?>
                            <li class="breadcrumb-item ">

                                <a href="<?= $link ?>">{{ ucwords(str_replace('-',' ',Request::segment($i)))}}</a>

                            </li>
                        @else

                            <li class="breadcrumb-item active">{{ucwords(str_replace('-',' ',Request::segment($i)))}}</li>

                        @endif
                    @endfor
                </ol>
            </div>
        </div>
    </div>
</div>