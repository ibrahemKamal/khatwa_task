<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    @include("layouts.partials.navigation")

    @include("layouts.partials.aside")


    <div class="content-wrapper">

        @include('layouts.partials.breadcrumbs')

        @yield('content')

    </div>

    @include('layouts.partials.footer')

</div>

</body>