<script>
    $(document).ready(function () {
        @foreach($errors->all() as $error)
        $(document).Toasts('create', {
            class: 'bg-danger',
            body: "{{$error}}",
            title: 'Error',
            icon: 'fas fa-exclamation-triangle',
        })
        @endforeach

        @foreach(\Illuminate\Support\Arr::wrap(session()->get('success',[])) as $msg)
        $(document).Toasts('create', {
            class: 'bg-success',
            body: "{{$msg}}",
            title: 'Success',
            icon: 'fas fa-exclamation-triangle',
        })
        @endforeach

    });

</script>