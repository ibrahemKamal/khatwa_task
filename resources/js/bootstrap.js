try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');
    window.moment  = require('moment');

    require('bootstrap');

    require('admin-lte');
} catch (e) {
    console.log(e);
}
