<?php

use App\Models\Company;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyLoginLogsTable extends Migration
{
    public function up()
    {
        Schema::create('company_login_logs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->foreignIdFor(Company::class)
                ->constrained()
                ->onDelete('cascade');

            $table->timestamp('created_at');

            $table->index('created_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('company_login_logs');
    }
}
