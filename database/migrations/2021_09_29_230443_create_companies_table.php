<?php

use App\Models\Company;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->unique();
            $table->string('tel');
            $table->string('address');
            $table->string('subdomain');
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('status', array_keys(Company::AVAILABLE_STATUSES))
                ->default(Company::STATUS_INACTIVE)
                ->comment(json_encode(Company::AVAILABLE_STATUSES));

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
