<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\CompanyLoginLog;
use App\Models\Product;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    public function run()
    {
        Company::factory(5)
            ->has(CompanyLoginLog::factory(3), 'loginLogs')
            ->has(Product::factory(3))
            ->create();
    }
}
