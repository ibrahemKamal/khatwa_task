<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // production seeders

        $this->call([
            AdminSeeder::class,
        ]);

        // seeders needed to testing/developing
        if (! app()->environment('production')) {
            $this->call([
                CompanySeeder::class,
            ]);
        }
    }
}
