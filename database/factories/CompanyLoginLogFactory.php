<?php

namespace Database\Factories;

use App\Models\CompanyLoginLog;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyLoginLogFactory extends Factory
{
    protected $model = CompanyLoginLog::class;

    public function definition(): array
    {
        return [
            'company_id' => $this->faker->randomNumber(),
            'created_at' => $this->faker->dateTimeBetween('-5 days'),
        ];
    }
}
