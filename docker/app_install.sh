cp .env.laravel.example ../.env
cp .env.dusk.example ../.env.dusk.local
#docker-compose build app
docker-compose up -d --build --force-recreate --always-recreate-deps --remove-orphans
docker-compose exec -T app /bin/sh -c  "composer install && \
php artisan key:generate && \
php artisan migrate:fresh --seed && \
php artisan storage:link && \
php artisan dusk:chrome-driver && \
npm i && npm run prod "
#docker-compose stop

