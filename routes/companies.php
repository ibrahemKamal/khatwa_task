<?php
// Authentication Required Routes
Route::group(['middleware' => 'auth:company'], function () {

    //dashboard
    Route::get('/', 'Homepage\IndexController')
        ->name('homepage.index');

});


// Auth Routes
Route::group(['prefix' => 'auth', 'namespace' => 'Auth', 'as' => 'auth.'], function () {
    Route::get('login', 'LoginIndexController')
        ->name('login.index')
        ->middleware('guest:company');

    Route::post('login', 'PostLoginController')
        ->name('login.post')
        ->middleware('guest:company');

    Route::get('logout', 'LogoutController')
        ->name('logout')
        ->middleware('auth:company');
});
