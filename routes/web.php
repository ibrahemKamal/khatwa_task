<?php

/**
 * Admin Routes.
 */
Route::group(['namespace' => 'Admin', 'as' => 'admin.'], function () {

    // Authentication Required Routes
    Route::group(['middleware' => 'auth'], function () {

        //dashboard
        Route::get('/', 'Dashboard\IndexController')
            ->name('dashboard.index');

        // Companies
        Route::group(['prefix' => 'companies', 'namespace' => 'Companies', 'as' => 'company.'], function () {

            Route::get('', 'CompaniesIndexController')
                ->name('index');

            Route::get('create', 'CreateCompanyIndexController')
                ->name('create');

            Route::post('store', 'StoreCompanyController')
                ->name('store');

            Route::get('{company:name}/edit', 'EditCompanyIndexController')
                ->name('edit');

            Route::post('{company}/update', 'UpdateCompanyController')
                ->name('update');

            Route::get('{company:name}/details', 'ShowCompanyController')
                ->name('details');

            Route::delete('{company}/delete', 'DeleteCompanyController')
                ->name('delete');
        });

        // Products
        Route::group(['prefix' => 'products', 'namespace' => 'Products', 'as' => 'product.'], function () {

            Route::get('', 'ProductsIndexController')
                ->name('index');

            Route::get('create', 'CreateProductIndexController')
                ->name('create');

            Route::post('store', 'StoreProductController')
                ->name('store');

            Route::get('{product:name}/edit', 'EditProductIndexController')
                ->name('edit');

            Route::post('{product}/update', 'UpdateProductController')
                ->name('update');

            Route::get('{product:name}/details', 'ShowProductController')
                ->name('details');

            Route::delete('{product}/delete', 'DeleteProductController')
                ->name('delete');
        });
    });

    // Auth Routes
    Route::group(['prefix' => 'auth', 'namespace' => 'Auth', 'as' => 'auth.'], function () {
        Route::get('login', 'LoginIndexController')
            ->name('login.index')
            ->middleware('guest');

        Route::post('login', 'PostLoginController')
            ->name('login.post')
            ->middleware('guest');

        Route::get('logout', 'LogoutController')
            ->name('logout')
            ->middleware('auth');
    });
});
