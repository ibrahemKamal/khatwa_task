<?php

namespace App\Http\Controllers\Companies\HomePage;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function __invoke()
    {
        $user = auth('company')->user();
        $user->loadCount('products');
        return view('tenant.pages.profile.index', compact('user'));
    }
}