<?php

namespace App\Http\Controllers\Companies\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;

class LoginIndexController extends Controller
{
    public function __invoke(): View
    {
        $login_url = route('companies.auth.login.post');
        return view('pages.auth.login-index', compact('login_url'));
    }
}
