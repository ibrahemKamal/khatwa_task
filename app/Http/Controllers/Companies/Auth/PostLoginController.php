<?php

namespace App\Http\Controllers\Companies\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Companies\Auth\PostLoginRequest;
use App\Services\MultiTenant;
use Illuminate\Support\Facades\Request;

class PostLoginController extends Controller
{
    public function __invoke(PostLoginRequest $request)
    {
        $credentials = $request->only(['email', 'password']) + ['subdomain' => MultiTenant::currentSubdomain()];
        if (!\Auth::guard('company')->attempt($credentials, $request->remember_me)) {
            return back()->withInput()->withErrors('Invalid Credentials');
        }

        return redirect(MultiTenant::currentTenantHomePage());
    }
}
