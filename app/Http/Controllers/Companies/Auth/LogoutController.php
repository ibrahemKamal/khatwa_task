<?php

namespace App\Http\Controllers\Companies\Auth;

use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
    public function __invoke()
    {
        auth('company')->logout();

        return redirect()->route('companies.auth.login.index');
    }
}
