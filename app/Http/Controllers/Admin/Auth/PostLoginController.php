<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Auth\PostLoginRequest;
use App\Services\MultiTenant;

class PostLoginController extends Controller
{
    public function __invoke(PostLoginRequest $request)
    {
        if (! \Auth::attempt($request->only(['email', 'password']), $request->remember_me)) {
            return back()->withInput()->withErrors('Invalid Credentials');
        }

        return redirect(MultiTenant::currentTenantHomePage());
    }
}
