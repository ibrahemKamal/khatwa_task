<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Product;

class ShowProductController extends Controller
{
    public function __invoke(Product $product)
    {
        return view('pages.products.details', compact('product'));
    }
}