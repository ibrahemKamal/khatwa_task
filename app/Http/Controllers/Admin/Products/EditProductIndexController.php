<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Product;

class EditProductIndexController extends Controller
{
    public function __invoke(Product $product)
    {
        $companies = Company::all('id', 'name');
        $product->load('company:name');
        return view('pages.products.edit', compact('product', 'companies'));
    }
}