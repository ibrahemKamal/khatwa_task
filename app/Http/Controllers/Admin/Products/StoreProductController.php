<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Products\StoreProductRequest;
use App\Models\Product;

class StoreProductController extends Controller
{
    public function __invoke(StoreProductRequest $request)
    {
        Product::create($request->validated());
        return redirect()->route('admin.product.index')->withSuccess('Added Successfully');
    }
}