<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Product;

class ProductsIndexController extends Controller
{
    public function __invoke()
    {
        $search_value = request()->get('q');
        $products = Product::textSearch($search_value)
            ->latest('id')
            ->with(['company:id,name'])
            ->paginate(15)
            ->withQueryString();
        return view('pages.products.index', compact('products', 'search_value'));
    }
}