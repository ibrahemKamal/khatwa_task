<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Models\Company;

class CreateProductIndexController extends Controller
{
    public function __invoke()
    {
        $companies = Company::all('id', 'name');
        return view('pages.products.create', compact('companies'));
    }
}