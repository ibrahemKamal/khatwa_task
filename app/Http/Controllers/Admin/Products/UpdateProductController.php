<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Products\UpdateProductRequest;
use App\Models\Product;

class UpdateProductController extends Controller
{
    public function __invoke(Product $product, UpdateProductRequest $request)
    {
        $product->update($request->validated());
        return redirect()->route('admin.product.index')->withSuccess('Updated Successfully');
    }
}