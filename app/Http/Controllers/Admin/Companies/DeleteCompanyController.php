<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Http\Controllers\Controller;
use App\Models\Company;

class DeleteCompanyController extends Controller
{
    public function __invoke(Company $company)
    {
        $company->delete();
        return redirect()->route('admin.company.index')->withSuccess('Deleted Successfully');
    }
}