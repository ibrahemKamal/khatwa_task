<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Companies\UpdateCompanyRequest;
use App\Models\Company;

class UpdateCompanyController extends Controller
{
    public function __invoke(Company $company, UpdateCompanyRequest $request)
    {
        $company->update($request->validated());
        return redirect()->route('admin.company.index')->withSuccess('Updated Successfully');
    }
}