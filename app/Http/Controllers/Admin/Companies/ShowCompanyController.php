<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Http\Controllers\Controller;
use App\Models\Company;

class ShowCompanyController extends Controller
{
    public function __invoke(Company $company)
    {
        $company->loadCount('products');
        return view('pages.companies.details', compact('company'));
    }
}