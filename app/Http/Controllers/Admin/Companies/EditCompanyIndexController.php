<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Http\Controllers\Controller;
use App\Models\Company;

class EditCompanyIndexController extends Controller
{
    public function __invoke(Company $company)
    {
        $statuses = Company::AVAILABLE_STATUSES;
        return view('pages.companies.edit', compact('company', 'statuses'));
    }
}