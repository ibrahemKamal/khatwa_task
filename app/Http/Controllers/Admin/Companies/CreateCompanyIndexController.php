<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Http\Controllers\Controller;
use App\Models\Company;

class CreateCompanyIndexController extends Controller
{
    public function __invoke()
    {
        $statuses = Company::AVAILABLE_STATUSES;
        return view('pages.companies.create', compact('statuses'));
    }
}