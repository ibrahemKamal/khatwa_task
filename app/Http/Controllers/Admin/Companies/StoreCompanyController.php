<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Companies\StoreCompanyRequest;
use App\Models\Company;

class StoreCompanyController extends Controller
{
    public function __invoke(StoreCompanyRequest $request)
    {
        Company::create($request->validated());
        return redirect()->route('admin.company.index')->withSuccess('Added Successfully');
    }
}