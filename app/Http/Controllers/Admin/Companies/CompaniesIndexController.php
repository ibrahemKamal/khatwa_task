<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Http\Controllers\Controller;
use App\Models\Company;

class CompaniesIndexController extends Controller
{
    public function __invoke()
    {
        $search_value = request()->get('q');
        $companies = Company::textSearch($search_value)
            ->latest('id')
            ->paginate(15, ['id', 'name', 'subdomain', 'status'])
            ->withQueryString();
        return view('pages.companies.index', compact('companies', 'search_value'));
    }
}