<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\CompanyLoginLog;
use Illuminate\Contracts\View\View;

class IndexController extends Controller
{
    public function __invoke(): View
    {
        $date_from = request('date_from') ?? today()->subMonth()->toDateString();
        $date_to = request('date_to') ?? today()->toDateString();
        $chart_data = $this->buildChartData($date_from, $date_to);
        return view('pages.dashboard.index', compact('chart_data', 'date_from', 'date_to'));
    }

    protected function buildChartData($date_from, $date_to)
    {
        $companies_logins = CompanyLoginLog::query()
            ->selectRaw('DATE(created_at) as date, COUNT(*) as count')
            ->groupBy('date')
            ->orderBy('date')
            ->whereBetween('created_at', [$date_from, $date_to])
            ->get();
        return collect([
             'labels' =>  $companies_logins->pluck('date')->toArray(),
            'data' => $companies_logins->pluck('count')->toArray()
        ]);
    }
}
