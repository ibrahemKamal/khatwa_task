<?php

namespace App\Http\Middleware;

use App\Models\Company;
use Closure;
use Illuminate\Http\Request;

class CheckTenantMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        // Extract the subdomain from URL.
        [$subdomain] = explode('.', $request->getHost(), 2);

        // Retrieve requested tenant's info from database. If not found, abort the request.
        Company::where('subdomain', $subdomain)->firstOrFail();


        return $next($request);
    }
}