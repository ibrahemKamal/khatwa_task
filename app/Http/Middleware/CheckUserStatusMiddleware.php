<?php

namespace App\Http\Middleware;

use App\Models\Company;
use Closure;
use Illuminate\Http\Request;

class CheckUserStatusMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->user() && $request->user()->status != Company::STATUS_ACTIVE) {
            auth('company')->logout();
            return redirect()->route('companies.auth.login.index')->withErrors('Account status is inactive');
        }
        return $next($request);
    }
}