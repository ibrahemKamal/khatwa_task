<?php

namespace App\Http\Middleware;

use App\Services\MultiTenant;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class SetSubdomainMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        URL::defaults([
            'subdomain' => MultiTenant::currentSubdomain()
        ]);
        return $next($request);
    }
}