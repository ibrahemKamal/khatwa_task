<?php

namespace App\Http\Requests\Admin\Companies;

use App\Models\Company;
use App\Rules\SubdomainRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateCompanyRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|max:50|string|unique:companies,name,' . $this->route('company')->id,
            'email' => 'required|max:50|email|unique:companies,email,' . $this->route('company')->id,
            'tel' => 'required|starts_with:01|max:11|min:10',
            'address' => 'required|max:191',
            'subdomain' => [
                'required',
                'max:20',
                'string',
                new SubdomainRule()
            ],
            'password' => 'nullable|confirmed|string|min:8|max:100',
            'status' => [
                'required',
                Rule::in(array_keys(Company::AVAILABLE_STATUSES))
            ]
        ];
    }


    public function authorize(): bool
    {
        return true;
    }
}