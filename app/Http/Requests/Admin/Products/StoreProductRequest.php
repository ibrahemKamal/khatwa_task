<?php

namespace App\Http\Requests\Admin\Products;

use App\Models\Company;
use App\Rules\SubdomainRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreProductRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|max:50|string',
            'price' => 'required|min:1|numeric',
            'quantity' => 'required|min:1|numeric',
            'company_id' => 'required|exists:companies,id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}