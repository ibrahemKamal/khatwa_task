<?php

namespace App\Http\Requests\Companies\Auth;

use Illuminate\Foundation\Http\FormRequest;

class PostLoginRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => 'required|email|max:50|exists:companies,email',
            'password' => 'required',
            'remember_me'=>'nullable|boolean',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}