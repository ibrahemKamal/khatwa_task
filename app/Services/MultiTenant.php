<?php

namespace App\Services;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class MultiTenant
{
    public static function isAdminRoute(): bool
    {
        return Str::of(Route::currentRouteName())->startsWith('admin.');
    }

    public static function currentTenantHomePage(): string
    {
        return self::isAdminRoute() ? \route('admin.dashboard.index') : \route('companies.homepage.index');
    }

    /**
     * @return object|string|null
     */
    public static function currentSubdomain()
    {
        return \Route::current()->parameter('subdomain');
    }
}
