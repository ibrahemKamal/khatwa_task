<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class Company extends Authenticatable
{
    use HasFactory;

    const STATUS_ACTIVE = 1;

    const STATUS_INACTIVE = 2;

    const AVAILABLE_STATUSES = [
        self::STATUS_ACTIVE => 'active',
        self::STATUS_INACTIVE => 'inactive',
    ];

    const STATUSES_BADGES = [
        self::STATUS_ACTIVE => 'badge-success',
        self::STATUS_INACTIVE => 'badge-danger',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name', 'email', 'password', 'remember_token', 'tel', 'address', 'subdomain', 'status'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    /**
     * Relations
     */
    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    public function loginLogs(): HasMany
    {
        return $this->hasMany(CompanyLoginLog::class);
    }

    /**
     * Scopes
     *
     * @param $query
     * @param $value
     *
     * @return mixed
     */

    public function scopeTextSearch($query, $value)
    {
        $searchable_columns = ['name', 'subdomain', 'email'];
        return $query->when($value, function ($q) use ($value, $searchable_columns) {
            foreach ($searchable_columns as $column) {
                $q->orWhere($column, "like", "%$value%");
            }
        });
    }

    /**
     * Accessors
     */

    public function getFormattedStatusAttribute(): string
    {
        return self::AVAILABLE_STATUSES[$this->status];
    }

    public function getStatusBadgeAttribute(): string
    {
        return self::STATUSES_BADGES[$this->status];
    }


    /**
     * Mutators
     */

    /**
     * @param $value
     */
    public function setSubdomainAttribute($value): void
    {
        $this->attributes['subdomain'] = strtolower($value);
    }

    /**
     * @param $value
     */
    public function setPasswordAttribute($value): void
    {
        if ($value) {
            if (strpos($value, '$2y$') !== 0) {
                $this->attributes['password'] = bcrypt($value);
            } else {
                $this->attributes['password'] = $value;
            }
        }
    }
}
