<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CompanyLoginLog extends Model
{
    use HasFactory;

    protected $fillable = ['company_id'];

    // disabling updated_at timestamp as it wont be used
    const UPDATED_AT = null;

    /**
     * Relations.
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }
}
