<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'price', 'quantity', 'company_id'];

    /**
     * Relations.
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }


    /**
     * Scopes
     *
     * @param $query
     * @param $value
     *
     * @return mixed
     */

    public function scopeTextSearch(Builder $query, $value)
    {
        $searchable_columns = ['name', 'price', 'quantity'];
        return $query->when($value, function ($q) use ($value, $searchable_columns) {
            foreach ($searchable_columns as $column) {
                $q->orWhere($column, "like", "%$value%");
            }
            $q->orWhereRelation('company', 'name', 'like', "%$value%");
        });
    }
}
