# Khatwa Task


### Requirements:

- docker
- docker-compose


### Seeded data:

- [Please view the seeded data guide](SEEDED.md)

### Installation:

1.  Clone this repository
2.  `cd docker`
3.  `sh app_install.sh`


### Usage:

- ##### Start the app:

1.  `cd docker`
2.  - `sh app_start.sh`

- starts nginx web server: http://localhost:8000
- starts mysql server: (port 3306)

- ##### Stop the app:

1.  `cd docker`
2.  `sh app_stop.sh`

- ##### Executing commands inside the app container:

1.  `cd docker`
2.  `sh attach_terminal.sh`

- ##### For database migration and seeding
- phpMyAdmin is available on : http://localhost:8088
    - username: admin
    - password: admin
- root access : 
  - username : root
  - password : root
    

- database is already seeded with the app_install command
  

- if u don't want the dummy data just change environment to production

- ##### For Testing
- Laravel Dusk has been used for browser testing


- to run test just run `php artisan dusk` inside the docker container




