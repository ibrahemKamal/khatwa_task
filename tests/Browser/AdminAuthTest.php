<?php

namespace Tests\Browser;

use App\Models\Admin;
use Illuminate\Support\Facades\Artisan;
use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;

class AdminAuthTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_admin_can_login()
    {
        $this->browse(function (Browser $browser) {
            Artisan::call('db:seed', ['class' => 'AdminSeeder']);
            $browser->visit(route('admin.auth.login.index'))
                ->type('email', 'admin@admin.com')
                ->type('password', 'admin')
                ->click('button[type="submit"]')
                ->assertRouteIs('admin.dashboard.index');
        });
    }
}