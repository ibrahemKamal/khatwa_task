<?php

namespace Tests\Browser;

use App\Models\Admin;
use App\Models\Company;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;

/**
 * @property \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model admin
 * @property \Faker\Generator faker
 */
class CompanyTest extends DuskTestCase
{
    use DatabaseMigrations, WithFaker;


    protected function setUp(): void
    {
        parent::setUp();
        $this->admin = Admin::factory()->baseAdmin()->create();
    }


    /** @test */
    public function can_view_index_of_companies()
    {
        $company = Company::factory()->create();
        $this->browse(function (Browser $browser) use ($company) {
            $browser->
            loginAs($this->admin)
                ->visit(route('admin.company.index'))
                ->assertsee($company->name)
                ->assertSee($company->subdomain)
                ->assertSee($company->status);
        });
    }

    /** @test */
    public function can_create_company()
    {
        $this->browse(function (Browser $browser) {
            $browser->
            loginAs($this->admin)
                ->visit(route('admin.company.index'))
                ->click('@create')
                ->assertRouteIs('admin.company.create')
                ->type('name', $this->faker->word)
                ->type('tel', '01111111111')
                ->type('address', $this->faker->address)
                ->type('subdomain', $this->faker->word)
                ->type('email', $this->faker->safeEmail)
                ->type('password', 'password')
                ->select('status', Company::STATUS_ACTIVE)
                ->type('password', 'password')
                ->type('password_confirmation', 'password')
                ->click('@submit-button')
                ->assertRouteIs('admin.company.index');
        });
    }

    /** @test */
    public function can_update_company()
    {
        $company = Company::factory()->create();
        $this->browse(function (Browser $browser) use ($company) {
            $browser->
            loginAs($this->admin)
                ->visit(route('admin.company.edit', $company))
                ->assertSee($company->name)
                ->type('name', 'updated name')
                ->click('@submit-button')
                ->assertRouteIs('admin.company.index')
                ->assertSee('updated name');
        });
    }


    /** @test */
    public function can_view_company()
    {
        $company = Company::factory()->create();
        $this->browse(function (Browser $browser) use ($company) {
            $browser->
            loginAs($this->admin)
                ->visit(route('admin.company.index'))
                ->click('@view')
                ->assertSee($company->name)
                ->assertRouteIs('admin.company.details', $company);
        });
    }

    /** @test */
    public function can_delete_company()
    {
        $company = Company::factory()->create();
        $this->browse(function (Browser $browser) use ($company) {
            $browser->
            loginAs($this->admin)
                ->visit(route('admin.company.index'))
                ->click('@delete')
                ->whenAvailable('.modal', function ($modal) {
                    $modal->assertSee('Delete Confirmation')
                        ->click('@delete-button');
                })
                ->assertRouteIs('admin.company.index')
                ->assertDontSee($company->name);
        });
    }

    /** @test */
    public function can_search_companies()
    {
        $first_company = Company::factory([
            'name' => 'test'
        ])->create();
        $second_company = Company::factory([
            'name' => 'another company'
        ])->create();
        $this->browse(function (Browser $browser) use ($first_company,$second_company) {
            $browser->
            loginAs($this->admin)
                ->visit(route('admin.company.index'))
                ->type('q','tes')
                ->click('@submit-search')
                ->assertSee($first_company->name)
                ->assertDontSee($second_company->name);
        });
    }
}